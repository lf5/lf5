##diätentabelle
##REZEPT ZU ERNÄHRUNGSKATEGORIE (VEGAN, VEGETARISCH ETC.)


create table DIÄT (
	REZEPTID	INT(11) NOT NULL,
    KATEGORIEID	INT(11) NOT NULL,
	FOREIGN KEY (REZEPTID) REFERENCES REZEPTE(REZEPTID),
    FOREIGN KEY (KATEGORIEID) REFERENCES ERNÄHRUNGSKATEGORIE(KATEGORIEID)
);

INSERT INTO DIÄT (REZEPTID,KATEGORIEID)
	VALUE 		(1,3),
				(1,5),
                (1,6),
                (2,5),
                (2,6),
                (3,1),
                (3,2),
                (3,5),
                (4,2),
                (5,1),
                (5,2),
                (5,5),
                (5,6)
                ;
