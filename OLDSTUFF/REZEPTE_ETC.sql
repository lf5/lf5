##REZEPTE

CREATE TABLE REZEPT (
	REZEPTID	INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	REZEPTNAME	VARCHAR(30)
);
INSERT INTO REZEPT (REZEPTNAME)
VALUE 	("LACHSLASAGNE"),
		    ("THAICURRY MIT CHICKEN"),
        ("KARTOFFELSUPPE"),
        ("MILCHREIS MIT APFELMUS"),
        ("COUSCOUS SALAT")
        ;

##ERNÄHRUNGSKATEGORIE

CREATE TABLE ERNÄHRUNGSKATEGORIE (
	KATEGORIEID 	INT(11) NOT NULL PRIMARY KEY auto_increment,
    ERNÄHRUNGSTYP	VARCHAR (30) NOT NULL
);
insert into ERNÄHRUNGSKATEGORIE (ERNÄHRUNGSTYP)

values 	("Vegan"),
		      ("Vegetarisch"),
        ("Pescetarisch"),
        ("Frutarisch"),
        ("Low-Carb"),
        ("High Protein")
        ;

##ALLERGIEN


CREATE TABLE ALLERGIEN (
	ALLERGIEID INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ALLERGIE VARCHAR (30)
);
INSERT INTO ALLERGIEN (ALLERGIE)
VALUES 	("Laktose"),
        ("Gluten"),
        ("Erdnüsse"),
        ("Fisch"),
        ("Krustentiere")
        ;


##REZEPTKOMPONENTEN


        create table REZEPTKOMPONENTEN (
        	REZEPTID	INT(11) NOT NULL,
            ZUTATENNR		INT(11) NOT NULL,
            FOREIGN KEY (REZEPTID) REFERENCES REZEPT(REZEPTID),
            FOREIGN KEY (ZUTATENNR) REFERENCES ZUTAT(ZUTATENNR)
        );

        INSERT INTO REZEPTKOMPONENTEN (REZEPTID, ZUTATENNR)
        	VALUES		(1,3001),
        				(1,3002),
                        (1,3003),
                        (1,7043),
                        (2,1001),
                        (2,1002),
                        (2,1005),
                        (2,1009),
                        (2,1011),
                        (3,1002),
                        (3,1005),
                        (3,1006),
                        (3,1008),
                        (3,1012),
                        (3,5001),
                        (3,7043),
                        (3,9001),
                        (4,3001),
                        (4,3003),
                        (5,1001),
                        (5,1003),
                        (5,1012),
                        (5,6408),
                        (5,7043)
                        ;






COMMIT;
