-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 07. Okt 2021 um 17:48
-- Server-Version: 10.4.14-MariaDB
-- PHP-Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `team1`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rezeptekategorie`
--

CREATE TABLE `rezeptekategorie` (
  `RezeptNr` int(11) NOT NULL,
  `KategorieNr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `rezeptekategorie`
--

INSERT INTO `rezeptekategorie` (`RezeptNr`, `KategorieNr`) VALUES
(1, 5),
(2, 5),
(2, 6),
(3, 1),
(3, 2),
(3, 5),
(3, 6),
(3, 7),
(4, 1),
(4, 5),
(4, 7),
(5, 1),
(5, 2),
(5, 5),
(5, 6),
(5, 7);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `rezeptekategorie`
--
ALTER TABLE `rezeptekategorie`
  ADD PRIMARY KEY (`RezeptNr`,`KategorieNr`),
  ADD KEY `KategorieNr` (`KategorieNr`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `rezeptekategorie`
--
ALTER TABLE `rezeptekategorie`
  ADD CONSTRAINT `rezeptekategorie_ibfk_1` FOREIGN KEY (`RezeptNr`) REFERENCES `rezepte` (`RezeptNr`),
  ADD CONSTRAINT `rezeptekategorie_ibfk_2` FOREIGN KEY (`KategorieNr`) REFERENCES `ernährungskategorie` (`KategorieNr`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
