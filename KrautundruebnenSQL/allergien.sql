-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 07. Okt 2021 um 17:48
-- Server-Version: 10.4.14-MariaDB
-- PHP-Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `team1`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `allergien`
--

CREATE TABLE `allergien` (
  `AllergieNr` int(11) NOT NULL,
  `Allergie` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `allergien`
--

INSERT INTO `allergien` (`AllergieNr`, `Allergie`) VALUES
(1, 'Laktose'),
(2, 'Weizen'),
(3, 'Gluten'),
(4, 'Stärke'),
(5, 'Karotte');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `allergien`
--
ALTER TABLE `allergien`
  ADD PRIMARY KEY (`AllergieNr`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `allergien`
--
ALTER TABLE `allergien`
  MODIFY `AllergieNr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
