-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 07. Okt 2021 um 17:49
-- Server-Version: 10.4.14-MariaDB
-- PHP-Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `team1`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rezeptezutaten`
--

CREATE TABLE `rezeptezutaten` (
  `REZEPTNR` int(11) NOT NULL,
  `ZUTATENNR` int(11) NOT NULL,
  `MENGE` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `rezeptezutaten`
--

INSERT INTO `rezeptezutaten` (`REZEPTNR`, `ZUTATENNR`, `MENGE`) VALUES
(1, 3003, 1),
(2, 1009, 1),
(3, 1002, 1),
(3, 1005, 1),
(3, 1006, 5),
(3, 1008, 1),
(3, 5001, 1),
(3, 7043, 4),
(4, 3001, 1),
(4, 3003, 1),
(5, 1001, 1),
(5, 1003, 3),
(5, 6408, 1),
(5, 7043, 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `rezeptezutaten`
--
ALTER TABLE `rezeptezutaten`
  ADD PRIMARY KEY (`REZEPTNR`,`ZUTATENNR`),
  ADD KEY `ZUTATENNR` (`ZUTATENNR`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `rezeptezutaten`
--
ALTER TABLE `rezeptezutaten`
  ADD CONSTRAINT `rezeptezutaten_ibfk_1` FOREIGN KEY (`REZEPTNR`) REFERENCES `REZEPTE` (`REZEPTNR`),
  ADD CONSTRAINT `rezeptezutaten_ibfk_2` FOREIGN KEY (`ZUTATENNR`) REFERENCES `ZUTAT` (`ZUTATENNR`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
