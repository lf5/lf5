-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 07. Okt 2021 um 17:48
-- Server-Version: 10.4.14-MariaDB
-- PHP-Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `team1`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ernährungskategorie`
--

CREATE TABLE `ernährungskategorie` (
  `KategorieNr` int(11) NOT NULL,
  `Bezeichnung` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `ernährungskategorie`
--

INSERT INTO `ernährungskategorie` (`KategorieNr`, `Bezeichnung`) VALUES
(1, 'vegetarisch'),
(2, 'vegan'),
(3, 'low-carb'),
(4, 'frutarisch'),
(5, 'ohne Gentechnik'),
(6, 'laktosefrei'),
(7, 'glutenfrei');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `ernährungskategorie`
--
ALTER TABLE `ernährungskategorie`
  ADD PRIMARY KEY (`KategorieNr`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
