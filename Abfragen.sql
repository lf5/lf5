/*
##FUNKTIONIERT NICHT
##ABFRAGE NACH REZEPTEN OHNE BESTIMMTE ALLERGIE
SELECT DISTINCT REZEPT.REZEPTNAME FROM REZEPT
INNER JOIN REZEPTALLERGIE ON REZEPT.REZEPTID=REZEPTALLERGIE.REZEPTID
LEFT JOIN ALLERGIEN ON ALLERGIEN.ALLERGIEID=REZEPTALLERGIE.ALLERGIEID
WHERE ALLERGIEN.ALLERGIE NOT LIKE 'LAK%' || REZEPT.REZEPTNAME NOT IN (SELECT ALLERGIEID FROM REZEPTALLERGIE)
;
*/
##DIESE ABFRAGE FUNTKIONIERT WEIL DURCH LEFT JOIN SCHAUEN WIR DIE KOMPLETTE REZEPTTABELLE, ALSO MIT COUSCOUS SALAT, AN
##WILDCARD VOR '%LAK%' UM MÖGLICHE LEERTASTE IN DER TABELLE MIT AUFZUNEHMEN
/*SELECT * DISTINCT
    REZEPT.REZEPTNAME,
    rezeptallergie.allergieid
FROM
    REZEPT
        LEFT JOIN
    REZEPTALLERGIE ON  REZEPTALLERGIE.REZEPTID=REZEPT.REZEPTID
		LEFT JOIN
	ALLERGIEN ON ALLERGIEN.ALLERGIEID=REZEPTALLERGIE.ALLERGIEID
		WHERE ALLERGIEN.ALLERGIE IS NULL || ALLERGIEN.ALLERGIE NOT LIKE '%LAK%';*/

    SELECT distinct rezept.REZEPTID, rezept.REZEPTNAME
FROM
    REZEPT
        LEFT JOIN
    REZEPTALLERGIE ON  REZEPTALLERGIE.REZEPTID=REZEPT.REZEPTID
		LEFT JOIN
	ALLERGIEN ON ALLERGIEN.ALLERGIEID=REZEPTALLERGIE.ALLERGIEID
		WHERE ALLERGIEN.ALLERGIE IS NULL || ALLERGIEN.ALLERGIE NOT LIKE '%LAK%';


##Abfrage Zutaten nach Rezeptname

/*
SELECT zutat.bezeichnung
FROM zutat
CROSS JOIN rezept, rezeptkomponenten
where rezept.REZEPTID=rezeptkomponenten.Rezeptid &&
rezeptkomponenten.ZUTATENNR=zutat.ZUTATENNR &&
rezept.REZEPTNAME like 'Cous%';*/

SELECT zutat.bezeichnung
FROM zutat
INNER JOIN rezeptkomponenten ON zutat.ZUTATENNR=rezeptkomponenten.ZUTATENNR
INNER JOIN rezept ON rezept.REZEPTID=rezeptkomponenten.REZEPTID
where rezept.REZEPTNAME like 'Cous%'
;


##Abfrage, Rezepte nach Ernährungskategorie

SELECT DISTINCT rezept.REZEPTNAME
FROM rezept
INNER JOIN diät ON diät.REZEPTID=rezept.REZEPTID
INNER JOIN Ernährungskategorie ON diät.KATEGORIEID=Ernährungskategorie.kategorieID
where Ernährungskategorie.ERNÄHRUNGSTYP like 'veg%'
;


##Abfrage, Auswahl Rezepte nach Zutat

SELECT rezept.REZEPTNAME
FROM rezept
INNER JOIN rezeptkomponenten ON rezeptkomponenten.REZEPTID=rezept.REZEPTID
INNER JOIN zutat ON rezeptkomponenten.ZUTATENNR=zutat.ZUTATENNR
where zutat.bezeichnung like '%Milch%'
;


##Abfrage, Durchschnitts Nährwert der Zutaten einer Bestellung eines Kunden

SELECT kunde.vorname,
      kunde.nachname,
      sum(zutat.Kalorien * bestellungzutat.menge) / count(bestellung.BestellNR) AS 'Nährwert Durchschnitt'
FROM zutat
INNER JOIN bestellungzutat ON zutat.ZUTATENNR=bestellungzutat.ZUTATENNR
INNER JOIN bestellung ON bestellung.BESTELLNR=bestellungzutat.BESTELLNR
INNER JOIN kunde ON bestellung.KUNDENNR=kunde.KUNDENNR
where kunde.NACHNAME like 'ges%'
;


##Abfrage, Zutaten, die keinem Rezept zugeordnet sind

SELECT zutat.bezeichnung
FROM zutat
LEFT JOIN rezeptkomponenten ON zutat.ZUTATENNR=rezeptkomponenten.ZUTATENNR
WHERE rezeptkomponenten.ZUTATENNR IS NULL
;


##Abfrage zusatz: Gesamtwert Bestellungen nach Kunde

SELECT sum(bestellung.RECHNUNGSBETRAG)
FROM bestellung
INNER JOIN KUNDENNR ON bestellung.KUNDENNR=kunde.KUNDENNR
WHERE kunde.NACHNAME like '%'
;


##Abfrage zusatz: Auflistung Zutaten für einen Kunden

SELECT bestellung.BESTELLNR, zutat.Bezeichnung
FROM zutat
INNER JOIN bestellungzutat ON bestellungzutat.ZUTATENNR=zutat.ZUTATENNR
INNER JOIN bestellung ON bestellung.BESTELLNR=bestellungzutat.BESTELLNR
INNER JOIN kunde ON bestellung.KUNDENNR=kunde.KUNDENNR
where kunde.Nachname like 'well%'
;


##Abfrage Zustatz: Auflistung der Bestellungen eines betimmten Zeitraums

SELECT bestellung.BESTELLNR, kunde.KUNDENNR, kunde.vorname, kunde.NACHNAME, bestellung.RECHNUNGSBETRAG
FROM bestellung
INNER JOIN kunde ON bestellung.KUNDENNR=kunde.KUNDENNR
WHERE BESTELLDATUM BETWEEN 20200801 AND 20200815
;


##Abfrage Zusatz: nach Nährwert absteigend sortiert
SELECT
    ZUTAT.BEZEICHNUNG, ZUTAT.PROTEIN
FROM
    ZUTAT
WHERE
    PROTEIN > 5
ORDER BY PROTEIN DESC
LIMIT 8;

##5 oder weniger zutaten

select distinct rezept.rezeptname
from rezept
inner join rezeptkomponenten ON rezept.rezeptid=rezeptkomponenten.Rezeptid
group by rezeptkomponenten.rezeptid
having count(rezeptkomponenten.rezeptid) <= 5;

##4 oder mehr Zutaten

select distinct rezept.rezeptname
from rezept
inner join rezeptkomponenten ON rezept.rezeptid=rezeptkomponenten.Rezeptid
group by rezeptkomponenten.rezeptid
having count(rezeptkomponenten.rezeptid) >= 4;

##DSGVO Löschung

##Insert in eine Löschungstabelle um Löschungsanträge zu verwalten

SET @kundennr = 2002;

SET FOREIGN_KEY_CHECKS=0;

INSERT INTO DSGVO_LÖSCHUNG (KUNDENNR, VORNAME, NACHNAME)
SELECT KUNDENNR, VORNAME, NACHNAME
FROM Kunde
WHERE kunde.KUNDENNR=@kundennr
;
INSERT INTO DSGVO_Archiv (bestellnr, bestelldatum, rechnungsbetrag)
SELECT bestellung.bestellnr, bestellung.BESTELLDATUM, bestellung.RECHNUNGSBETRAG FROM bestellung
where KUNDENNR=@kundennr
;
DELETE FROM bestellung
WHERE kundennr=@kundennr
;
DELETE FROM kunde
WHERE kundennr=@kundennr
;
SET FOREIGN_KEY_CHECKS=1;


##DSGVO anzeigen

SET @kundennr = 2002;

SELECT * FROM kunde
Left join bestellung ON bestellung.KUNDENNR=kunde.KUNDENNR
where kunde.KUNDENNR=@kundennr
