CREATE PROCEDURE lessthanfive(IN anzahl INT)

select distinct rezept.rezeptname
from rezept
inner join rezeptkomponenten ON rezept.rezeptid=rezeptkomponenten.Rezeptid
group by rezeptkomponenten.rezeptid
having count(rezeptkomponenten.rezeptid) <= anzahl;

CALL lessthanfive(5);
